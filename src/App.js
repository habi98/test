import React, {Component, Fragment} from 'react';
import './App.css';
import image from './assets/images/960x576_1.png'
import image2 from './assets/images/nc_ss19_620x960px_1.png'
import ProgressBar from "./component/ProgressBar/ProgressBar";
import {Button} from "reactstrap";

const styles = {
    position: 'fixed',
    top: '32px',
    right: '32px'
}
class App extends Component{
    state = {
        start: 0
    };

    progressBar = () => {
        let time = Math.round(3*1000/100);
      let interval = setInterval(() => {
            let start = this.state.start +1;
            this.setState({
                start: start
            });
            if (start >= 100) {
                clearInterval(interval)
            }
        }, time);
    };

    componentDidMount() {
        this.progressBar()
    }

    render() {
      return (
          <Fragment>
                  <div>
                      <img style={{width: "100%", height: '100%'}} src={image} alt="image"/>
                      {this.state.start >= 100 ? null :<ProgressBar value={this.state.start}/>}
                      {this.state.start >= 100 &&  <Button style={styles} color="primary">Close</Button>}

                  </div>
          </Fragment>
      );
  }
}

export default App;

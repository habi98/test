import React from 'react';
import {Progress} from "reactstrap";

const ProgressBar = (props) => {
    return (
        <Progress className="progress" value={props.value}>{props.value}%</Progress>
    );
};

export default ProgressBar;